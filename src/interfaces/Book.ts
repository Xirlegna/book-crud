export interface IBook {
  id: string;
  title: string;
  author: string;
  publisher: string;
}

export interface IBookFormData {
  title: string;
  author: string;
  publisher: string;
}
