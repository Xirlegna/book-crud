import { useCallback, useEffect, useState } from 'react';
import axios from 'axios';

export const useFetch = <T>(url: string, dependencies: any[] = []) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<any>();
  const [value, setValue] = useState<T | null>();

  const callbackMemorized = useCallback(() => {
    setLoading(true);

    axios
      .get(`http://localhost:8000/${url}`)
      .then((response) => {
        setValue(response.data);
      })
      .catch((error) => {
        setError(error);
      })
      .finally(() => {
        setLoading(false);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, dependencies);

  useEffect(() => {
    callbackMemorized();
  }, [callbackMemorized]);

  return { loading, error, value };
};
