import { useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';

import BookForm from '../../../components/BookForm/BookForm';
import { useFetch } from '../../../hooks';
import { IBook, IBookFormData } from '../../../interfaces/Book';

const BookEdit: React.FC = () => {
  const { bookId } = useParams();
  const navigate = useNavigate();

  const { loading, error, value } = useFetch<IBook>(`books/${bookId}`, [bookId]);

  if (error) {
    return <div>ERROR</div>;
  }

  if (!value || loading) {
    return <div>LOADING</div>;
  }

  const formSubmit = (values: IBookFormData) => {
    axios
      .put(`http://localhost:8000/books/${bookId}`, { ...values })
      .then(() => {
        console.log('-- UPDATE SUCCESS --');
        navigate('/books');
      })
      .catch((errors) => {
        console.log('-- UPDATE FAIL --', errors);
      });
  };

  return (
    <div>
      <h4>BookEdit {bookId}</h4>
      <button onClick={() => navigate(-1)}>go back</button>
      <BookForm book={value} formSubmit={formSubmit} />
    </div>
  );
};

export default BookEdit;
