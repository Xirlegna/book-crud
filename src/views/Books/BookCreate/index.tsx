import { useNavigate } from 'react-router-dom';
import axios from 'axios';

import BookForm from '../../../components/BookForm/BookForm';
import { IBookFormData } from '../../../interfaces/Book';

const BookCreate: React.FC = () => {
  const navigate = useNavigate();

  const formSubmit = (values: IBookFormData) => {
    axios
      .post('http://localhost:8000/books', { ...values })
      .then(() => {
        console.log('-- CREATE SUCCESS --');
        navigate('/books');
      })
      .catch((errors) => {
        console.log('-- CREATE FAIL --', errors);
      });
  };

  return (
    <div>
      <h4>Book Create</h4>
      <button onClick={() => navigate(-1)}>go back</button>
      <BookForm formSubmit={formSubmit} />
    </div>
  );
};

export default BookCreate;
