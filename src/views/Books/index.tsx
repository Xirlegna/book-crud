import { useNavigate } from 'react-router-dom';

import BookTable from '../../components/BookTable/BookTable';
import { useFetch } from '../../hooks';
import { IBook } from '../../interfaces/Book';

import CardViewIcon from '../../components/Icons/CardViewIcon';
import LineViewIcon from '../../components/Icons/LineViewIcon';

const BookList: React.FC = () => {
  const navigate = useNavigate();
  const { loading, error, value } = useFetch<IBook[]>('books');

  if (error) {
    return <div>ERROR</div>;
  }

  if (!value || loading) {
    return <div>LOADING</div>;
  }

  return (
    <div>
      <h4>Book List</h4>
      <button onClick={() => navigate('/books/create')}>Create Book</button>
      <LineViewIcon />
      <CardViewIcon />
      <BookTable books={value} />
    </div>
  );
};

export default BookList;
