import { useEffect, useState } from 'react';
import { Field } from 'formik';

import './text-input.scss';

interface TextInputProps {
  id: string;
  label: string;
  error: string | null;
  touched: boolean;
}

const TextInput: React.FC<TextInputProps> = ({ id, label, error, touched }) => {
  const [inputClass, setInputClass] = useState(['text-input__input']);

  useEffect(() => {
    if (touched && error) {
      setInputClass(['text-input__input', 'text-input__input--error']);
    } else {
      setInputClass(['text-input__input']);
    }
  }, [error]);

  return (
    <div className="text-input">
      <label htmlFor={id} className="text-input__label">
        {label}
      </label>
      <Field id={id} name={id} type="text" className={inputClass.join(' ')} />
      {touched && error && <div className="text-input__error">{error}</div>}
    </div>
  );
};

export default TextInput;
