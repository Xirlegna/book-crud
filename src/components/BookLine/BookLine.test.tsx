import { render, screen } from '../../utils/test-utils';

import { IBook } from '../../interfaces/Book';
import BookLine from './BookLine';

const book: IBook = {
  id: 'b1',
  title: 'B1 title',
  author: 'B1 author',
  publisher: 'B1 publisher',
};

describe('BookLine component', () => {
  describe('Layout', () => {
    beforeEach(() => {
      render(<BookLine book={book} />);
    });

    test.each([
      { field: 'title', value: book.title },
      { field: 'publisher', value: book.publisher },
    ])('displays $field value', ({ value }) => {
      const element = screen.getByText(value);

      expect(element).toBeInTheDocument();
    });

    test('has edit button', () => {
      const editButton = screen.getByRole('button', { name: /edit/i });

      expect(editButton).toBeInTheDocument();
    });
  });
});
