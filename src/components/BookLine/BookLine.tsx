import { Link } from 'react-router-dom';

import { IBook } from './../../interfaces/Book';
import './book-line.scss';

const BookLine: React.FC<{ book: IBook }> = ({ book }) => {
  return (
    <div className="book-line">
      <div>
        <p>{book.title}</p>
        <p>{book.publisher}</p>
      </div>
      <div>
        <Link
          className="btn"
          to={`/books/${book.id}`}
          role="button"
        >
          Edit
        </Link>
        <button className="btn">Delete</button>
      </div>
    </div>
  );
};

export default BookLine;
