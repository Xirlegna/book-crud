import { Link } from 'react-router-dom';
import './header.scss';

const Header: React.FC = () => {
  return (
    <div className="header">
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/books">Books</Link>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Header;
