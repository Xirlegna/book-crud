import React, { Suspense } from 'react';
import { Routes, Route } from 'react-router-dom';

const BookList = React.lazy(() => import('../../views/Books'));
const BookCreate = React.lazy(() => import('../../views/Books/BookCreate'));
const BookEdit = React.lazy(() => import('../../views/Books/BookEdit'));

const BookRoutes: React.FC = () => {
  return (
    <Routes>
      <Route
        index
        element={
          <Suspense fallback={null}>
            <BookList />
          </Suspense>
        }
      />
      <Route
        path="create"
        element={
          <Suspense fallback={null}>
            <BookCreate />
          </Suspense>
        }
      />
      <Route
        path=":bookId"
        element={
          <Suspense fallback={null}>
            <BookEdit />
          </Suspense>
        }
      />
    </Routes>
  );
};

export default BookRoutes;
