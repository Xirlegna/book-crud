import './line-view-icon.scss';

const LineViewIcon: React.FC = () => {
  return (
    <svg
      viewBox="0 0 25 25"
      xmlns="http://www.w3.org/2000/svg"
      className="line-view-icon"
    >
      <path d="M0 0H25V7H0V0ZM2 2V5H23V2H2ZM0 18H25V25H0V18ZM2 20V23H23V20H2ZM25 9H0V16H25V9ZM2 14V11H23V14H2Z" />
    </svg>
  );
};

export default LineViewIcon;
