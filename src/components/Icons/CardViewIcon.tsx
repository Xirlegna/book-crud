import './line-view-icon.scss';

const CardViewIcon: React.FC = () => {
  return (
    <svg
      viewBox="0 0 25 25"
      xmlns="http://www.w3.org/2000/svg"
      className="line-view-icon"
    >
      <path
        d="M0 0V11H11V0H0ZM2 9V2H9V9H2ZM14 14V25H25V14H14ZM16 23V16H23V23H16ZM0 14H11V25H0V14ZM2 16V23H9V16H2ZM14 0V11H25V0H14ZM16 9V2H23V9H16Z"
      />
    </svg>
  );
};

export default CardViewIcon;
