import BookLine from '../../components/BookLine/BookLine';
import { IBook } from '../../interfaces/Book';

import './book-table.scss';

interface BookTableProps {
  books: IBook[];
}

const BookTable: React.FC<BookTableProps> = ({ books }) => {
  return (
    <div className="book-table">
      {books.map((book) => (
        <BookLine key={book.id} book={book} />
      ))}
    </div>
  );
};

export default BookTable;
