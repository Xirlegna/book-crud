import * as Yup from 'yup';

const BookFormSchema = Yup.object().shape({
  title: Yup.string()
    .required('Title is required')
    .min(3, 'Title is too short')
    .max(100, 'Title is too long'),
  author: Yup.string()
    .required('Author is required')
    .min(3, 'Author is too short')
    .max(100, 'Author is too long'),
  publisher: Yup.string()
    .required('Publisher is required')
    .min(3, 'Publisher is too short')
    .max(100, 'Publisher is too long'),
});

export default BookFormSchema;
