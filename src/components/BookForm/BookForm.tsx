import { Formik, Form } from 'formik';

import BookFormSchema from './BookFormSchema';
import TextInput from '../Inputs/TextInput';
import { IBook, IBookFormData } from '../../interfaces/Book';

import './panel.scss';

interface BookFormProps {
  book?: IBook;
  formSubmit: (values: IBookFormData) => any;
}

const BookForm: React.FC<BookFormProps> = ({ book, formSubmit }) => {
  return (
    <Formik
      initialValues={{
        title: book?.title ?? '',
        author: book?.author ?? '',
        publisher: book?.publisher ?? '',
      }}
      validationSchema={BookFormSchema}
      onSubmit={(values) => formSubmit(values)}
    >
      {({ errors, touched }) => (
        <Form className="panel">
          <div>
            <TextInput
              id="title"
              label="Title"
              error={errors.title}
              touched={touched.title}
            />
          </div>
          <div>
            <TextInput
              id="author"
              label="Author"
              error={errors.author}
              touched={touched.author}
            />
          </div>
          <div>
            <TextInput
              id="publisher"
              label="Publisher"
              error={errors.publisher}
              touched={touched.publisher}
            />
          </div>
          <div className="panel__footer">
            <button type="submit" className="btn">
              Submit
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default BookForm;
