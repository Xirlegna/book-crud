import { render, screen, waitFor } from '../../utils/test-utils';
import userEvent from '@testing-library/user-event';

import { IBook } from '../../interfaces/Book';
import BookForm from './BookForm';

const book: IBook = {
  id: 'b1',
  title: 'B1 title',
  author: 'B1 author',
  publisher: 'B1 publisher',
};

const errorMessages = {
  title: {
    required: 'Title is required',
  },
  author: {
    required: 'Author is required',
  },
  publisher: {
    required: 'Publisher is required',
  },
};

describe('BookLine component', () => {
  describe('Layout', () => {
    beforeEach(() => {
      render(<BookForm book={book} formSubmit={() => {}} />);
    });

    test.each([
      { field: 'title', labelText: /title/i, value: book.title },
      { field: 'author', labelText: /author/i, value: book.author },
      { field: 'publisher', labelText: /publisher/i, value: book.publisher },
    ])(
      'has "$field" input field with correct value',
      ({ labelText, value }) => {
        const input = screen.getByLabelText(labelText);

        expect(input).toBeInTheDocument();
        expect(input).toHaveValue(value);
      }
    );

    test('has submit button', () => {
      const submitButton = screen.getByRole('button', { name: /submit/i });

      expect(submitButton).toBeInTheDocument();
    });
  });

  describe('Interactions', () => {
    test.each([
      { field: 'title', errorMessage: errorMessages.title.required },
      { field: 'author', errorMessage: errorMessages.author.required },
      { field: 'publisher', errorMessage: errorMessages.publisher.required },
    ])('"$field" must be required', async ({ errorMessage }) => {
      render(<BookForm formSubmit={() => {}} />);

      const submitButton = screen.getByRole('button', { name: /submit/i });

      userEvent.click(submitButton);

      await waitFor(() => {
        const errorMessageElement = screen.queryByText(errorMessage);

        expect(errorMessageElement).toBeInTheDocument();
      });
    });
  });
});
