import '@testing-library/jest-dom';
import { render, RenderOptions } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';

const AllTheProviders: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  return <Router>{children}</Router>;
};

const customRender = (
  ui: React.ReactElement,
  options?: Omit<RenderOptions, 'wrapper'>
) => render(ui, { wrapper: AllTheProviders, ...options });

export * from '@testing-library/jest-dom';
export * from '@testing-library/react';
export { customRender as render };
