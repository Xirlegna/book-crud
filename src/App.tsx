import React, { Suspense } from 'react';
import { Routes, Route } from 'react-router-dom';

import Header from './components/Header/Header';
import Home from './views/Home';

import BookRoutes from './components/Routes/BookRoutes';

const BookList = React.lazy(() => import('./views/Books'));
const BookCreate = React.lazy(() => import('./views/Books/BookCreate'));
const BookEdit = React.lazy(() => import('./views/Books/BookEdit'));

const App: React.FC = () => {
  return (
    <div>
      <Header />
      <main>
        <Routes>
          <Route path="" element={<Home />} />
          <Route path="books/*" element={<BookRoutes />} />
        </Routes>
      </main>
    </div>
  );
};

export default App;
