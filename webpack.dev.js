const { merge } = require('webpack-merge');

const commonConfig = require('./webpack.common');

module.exports = merge(commonConfig, {
  mode: 'development',
  devServer: {
    static: './dist',
    historyApiFallback: true,
  },
  module: {
    rules: [
      {
        test: /\.s[ac]ss?$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
    ],
  },
});
