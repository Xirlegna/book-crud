# Book admin

## Pre-requisites

- Install [Node.js](https://nodejs.org/en/) latest version

## Getting started

- Clone the repository

```
$ git clone  <git lab template url> <project_name>
```

- Install dependencies

```
cd <project_name>
npm install
```

- Run the project

```
npm run api:start
npm start
```

Navigate to `http://localhost:3000`
